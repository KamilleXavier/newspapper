<div class="footer"> 
    <br>
    <div class="container text-center">

        <a href="http://" style="color: white"><p>
            <img src="image/icon/icon-whatsappbp.svg" alt="icon whatsapp" width="90" height="40">
            <img src="image/icon/icon-insta..svg" alt="icon instagram" width="90" height="40">
            <img src="image/icon/icon-faceb.svg" alt="icon facebook" width="90" height="40"></p>
        </a>
        
        <div class="row">
            <div class="col">
                <a href="https://www.cnnbrasil.com.br/politica/ultimas-noticias/" style="color: white"><p>POLÍTICA</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/governo-lula/" style="color: white"><p>Governo Lula</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/camara-dos-deputados/" style="color: white"><p>Câmara dos Deputados</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/senado-federal/" style="color: white"><p>Senado Federal</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/supremo-tribunal-federal-stf/" style="color: white"><p>Supremo Tribunal Federal</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/policia-federal-pf/" style="color: white"><p>Polícia Federal</p></a>
            </div>

            <div class="col">
                <a href="http://" style="color: white"><p>CARROS</p></a>
                <a href="https://www.chevrolet.com.br/?ppc=GOOGLE_700000001285798_71700000112315312_58700008475309153_p77193207364_&gclid=Cj0KCQjw1OmoBhDXARIsAAAYGSGt0ZRUVjFm4WmRW9W6A-6mKm1PvfCmyPCPC0vDJjHNelKB9dNW32IaApR_EALw_wcB&gclsrc=aw.ds" style="color: white"><p>Chevrotet</p></a>
                <a href="https://www2.mercedes-benz.com.br/passengercars.html?group=all&subgroup=see-all&view=BODYTYPE" style="color: white"><p>Mercedez-benz</p></a>
                <a href="https://www.bmw.com.br/pt/index.html" style="color: white"><p>Bmw</p></a>
                <a href="https://www.toyota.com.br/" style="color: white"><p>Toyota</p></a>
                <a href="https://www.honda.com.br/automoveis/" style="color: white"><p>Honda</p></a>
            </div>

            <div class="col">
                <a href="https://www.cnnbrasil.com.br/economia/" style="color: white"><p>ECONOMIA</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/mercado/" style="color: white"><p>Mercado</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/investimentos/" style="color: white"><p>Investimentos</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/dolar/" style="color: white"><p>Dólar</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/euro/" style="color: white"><p>Euro</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/banco-central-bc/" style="color: white"><p>Banco Central</p></a>
            </div>

            <div class="col">
                <a href="https://www.cnnbrasil.com.br/esportes/" style="color: white"><p>ESPORTE</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/futebol/" style="color: white"><p>Futebol</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/volei/" style="color: white"><p>Volei</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/basquete/" style="color: white"><p>Basquete</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/xadrez/" style="color: white"><p>Xadrez</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/futsal/" style="color: white"><p>Futsal</p></a>
            </div>
        </div>
    </div>

    <hr style="color: white">

    <div class="container text-center">
        <div class="row">
            <div class="col">
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/ciencia-e-tecnologia/" style="color: white">CIENCIA E TECNOLOGIA</a>
                <a href="https://www.cnnbrasil.com.br/tecnologia/novas-tecnologias/" style="color: white"><p>Conhecimento</p></a>
                <a href="https://www.cnnbrasil.com.br/tecnologia/" style="color: white"><p>Rede Social</p></a>
                <a href="https://www.cnnbrasil.com.br/tecnologia/automacao-industrial/" style="color: white"><p>Mecânica</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/industria/" style="color: white"><p>Indústria</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/tecnologia-da-informacao/" style="color: white"><p>Informação</p></a>
            </div>

            <div class="col">
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/educacao/" style="color: white">EDUCAÇÃO</a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/ensino/" style="color: white"><p>Ensino</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/educacao-infantil/" style="color: white"><p>Educação infantil</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/ensino-medio/" style="color: white"><p>Novo ensino medio</p></a>
                <a href="https://www.cnnbrasil.com.br/nacional/novo-ensino-medio-sera-mais-flexivel-e-diversificado-para-os-alunos-diz-especialista/" style="color: white"><p>Itinerários</p></a>
                <a href="https://www.cnnbrasil.com.br/saude/disciplina-ajuda-a-melhorar-o-sono-em-momento-de-medo-do-futuro-diz-medico/" style="color: white"><p>Disciplina</p></a>
            </div>

            <div class="col" >
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/meio-ambiente/" style="color: white"> MEIO AMBIENTE</a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/natureza/" style="color: white"><p>Natureza</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/preservacao/" style="color: white"><p>Preservação</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/ecossistema/" style="color: white"><p>Ecossistema</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/reciclagem/" style="color: white"><p>Reciclagem</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/biodiversidade/" style="color: white"><p>Biodiversidade</p></a>
            </div>

            <div class="col">
                <a href="https://www.cnnbrasil.com.br/podcasts/" style="color: white"><p>PODCASTS</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/ww-podcast/" style="color: white"><p>WW bt</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/fatos-do-dia/" style="color: white"><p>Fatos do Dia</p></a>
                <a href="https://www.cnnbrasil.com.br/tudo-sobre/cnnpelomundo/" style="color: white"><p>CNN Pelo Mundo</p></a>
            </div> 
        </div>
    </div>
    <br><br>
    <hr style="color: white">

    <div style="text-align: center">
        <p style="color: white">&copy; <?php echo date("d/m/Y H:i:s");?> <br> Todos os direitos reservados. </p>
        <br><br><br>
    </div>
</div>

</body>
</html>